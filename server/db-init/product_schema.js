var mongoose = require("mongoose");

var product_schema = mongoose.Schema({
  product_name: { type: String, required: true },
  product_price: { type: Number, required: true },
  product_description: { type: String, required: true },
  product_category: { type: String, required: true },
  product_quantity: { type: Number, required: true }
});

var product_model = mongoose.model("product", product_schema);
