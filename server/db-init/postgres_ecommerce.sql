drop database if exists ecommerce;
create database ecommerce;

\c ecommerce;

create table products_info (
    product_id serial primary key,
    product_name varchar(30) not null,
    product_price  integer not null,
    product_description varchar(300),
    product_quantity integer not null,
    product_category varchar(30) not null
);  

create table user_info (
    user_id serial primary key,
    user_name varchar (30) not null,
    user_email varchar (30) not null,
    user_password varchar (300),
    user_phone integer not null,
    isAdmin boolean not null
); 
