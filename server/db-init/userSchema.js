var mongoose = require("mongoose");
var Joi = require("joi");

const user_schema = mongoose.Schema({
  user_name: { type: String, required: true },
  user_email: { type: String, required: true, unique: true },
  user_phone: { type: Number, required: true },
  user_password: { type: String, required: true },
  isAdmin: { type: Boolean }
});

var user_model = mongoose.model("user_model", user_schema);

function validateUser(user) {
  const user_schema = Joi.object().keys({
    user_name: Joi.string()
      .min(5)
      .max(20)
      .required(),
    user_email: Joi.string()
      .min(5)
      .max(20)
      .required(),
    user_phone: Joi.number()

      .max(10)
      .required(),

    user_password: Joi.string()
      .min(5)
      .max(100)
      .required()
  });
  return Joi.validate(user, user_schema);
}

exports.user = user;
exports.validateUser = validateUser;
