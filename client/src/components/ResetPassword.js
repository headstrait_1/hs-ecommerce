import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./componentCss/ResetPassword.css";

export default class ResetPassword extends Component {
  render() {
    return (
      <div className="login">
        <div className="form">
          <form>
            <h1>RESET PASSWORD</h1>

            <input type="email" placeholder="Email" required />
            <button class="otp"> SEND OTP</button>

            <br />
            <input type="password" placeholder="OTP" required />
            <br />
            <button class="next">
              <Link to="/Confirm">NEXT</Link>
            </button>
          </form>
        </div>
      </div>
    );
  }
}
