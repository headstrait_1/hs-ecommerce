import React, { Component } from "react";
import Navbar from "./navbar";
import { connect } from "react-redux";
import './addnewproduct.css'
class addnewproduct extends Component {
    state = {
        productname: "",
        category: "",
        description: "",
        price: "",
        quantity: ""
    };
    onSubmitClick = () => {
        let product = {
            productname: this.state.productname,
            category: this.state.category,
            description: this.state.description,
            price: this.state.price,
            quantity: this.state.quantity,
        };
    };

    handleInputChange = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
    };
    render() {
        return (
            <div class="container" align="center">
                <form onSubmit={this.onSubmitClick}>
                    <h1>ADD NEW PRODUCT</h1>
                    <br />
                    <br />

                    <input
                        name="Product Name"
                        placeholder="PRODUCT NAME"
                        value={this.state.productname}
                        onChange={this.handleInputChange}
                        required
                    />
                    <br />
                    <br />
                    <input
                        type="category"
                        name="category"
                        placeholder="CATEGORY"
                        value={this.state.category}
                        onChange={this.handleInputChange}
                        required
                    />
                    <br />
                    <br />
                    <input
                        type="description"
                        name="description"
                        placeholder="DESCRIPTION"
                        value={this.state.description}
                        onChange={this.handleInputChange}
                        required
                    />
                    <br />
                    <br />
                    <div class="card">
                        <input
                            type="price"
                            name="price"
                            placeholder="PRICE"
                            value={this.state.price}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <input
                            type="quantity"
                            name="quantity"
                            placeholder="QUANTITY"
                            value={this.state.quantity}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                    <button onSubmitClick a href="/">Add Product</button>
                </form>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    users: state.userReducer.users
});

export default connect(
    mapStateToProps,
    { addnewproduct }
)(addnewproduct);
