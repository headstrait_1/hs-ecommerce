import React, { Component } from "react";
//import axios from "axios";
import { connect } from "react-redux";
// import navBar from "./navBar";

import DisplayProducts from "./displayProducts";
import { getProducts } from "../Actions/productHomeAction";

class productsHome extends Component {
  state = {
    name: "",
    price: "",
    description: "",
    category: ""
  };

  componentWillMount() {
    this.props.getProducts();
  }
  render() {
    console.log("passsing products", this.props.productDetails);
    return (
      <div>
        <DisplayProducts products={this.props.productDetails} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  productDetails: state.productHomeReducer.productDetails
});

export default connect(
  mapStateToProps,
  { getProducts }
)(productsHome);
