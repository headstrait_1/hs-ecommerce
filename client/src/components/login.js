import React, { Component } from "react";
import Navbar from "./navbar";
import "./componentCss/login.css";

import { connect } from "react-redux";
class login extends Component {
  state = {
    email: "",
    password: ""
  };

  handleInputChange = event => {
    const { value, name } = event.target;
    this.setState({
      [name]: value
    });
  };

  onLoginClick = () => {
    let user = {
      email: this.state.email,
      password: this.state.password
    };
    // this.props.loginUsers(user);
  };
  render() {
    return (
      <div id="container">
        <form onSubmit={this.onLoginClick}>
          <h1>LOG IN</h1>
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={this.state.email}
            onChange={this.handleInputChange}
            required
          />
          <br />
          <br />
          <input
            type="password"
            name="password"
            placeholder="Password"
            value={this.state.password}
            onChange={this.handleInputChange}
            required
          />
          <br />
          <br />
          <input type="button" value="LOGIN" />
          <br />
          <a id="link" href="/resetp">
            Forgot Password
          </a>
          <br />
          <br />
          <br /> <br />
          <hr></hr>
          <br />
          <p>Not a Member ?</p>
          <input id="sig" type="button" value="SIGN UP" />
        </form>
      </div>
    );
  }
}
const mapStateToProps = state => ({
  users: state.userReducer.users
});

export default connect(
  mapStateToProps,
  {}
)(login);
