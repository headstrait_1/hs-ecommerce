import React, { Component } from "react";
import Navbar from "./navbar";
import { connect } from "react-redux";
import './registerpage.css'
class registerpage extends Component {
    state = {
        fullname: "",
        email: "",
        password: "",
        confirmpassword: "",
        otp: ""
    };
    onSubmitClick = () => {
        let newuser = {
            fullname: this.state.fullname,
            confirmpassword: this.state.confirmpassword,
            email: this.state.email,
            password: this.state.password,
            otp: this.state.otp,
            phone: this.state.phone
        };
        this.props.registerUser(newuser);
        this.props.verifyOtp(newuser);
    };

    handleInputChange = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
    };
    render() {
        return (
            <div class="container" align="center">
                <div class="card" >
                    <form onSubmit={this.onSubmitClick}>
                        <h1>CREATE ACCOUNT</h1>
                        <br />
                        <br />

                        <input
                            name="fullname"
                            placeholder="FULL NAME"
                            value={this.state.fullname}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <input
                            type="email"
                            name="email"
                            placeholder="EMAIL"
                            value={this.state.email}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <input
                            type="password"
                            name="Password"
                            placeholder="PASSWORD"
                            value={this.state.password}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <input
                            type="confirm password"
                            name="confirm password"
                            placeholder="CONFIRM PASSWORD"
                            value={this.state.confirmpassword}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <input
                            type="phone"
                            name="phone"
                            placeholder="PHONE"
                            value={this.state.phone}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <input
                            type="otp"
                            name="otp"
                            placeholder="OTP"
                            value={this.state.otp}
                            onChange={this.handleInputChange}
                            required
                        />
                        <br />
                        <br />
                        <input type="submit" value="SIGN UP" bgcolor="#2EC4B6" />
                        <br />
                        <br />
                        <br />
                        <br />
                        <text>ALREADY HAVE AN ACCOUNT?<button a href="/login">Log in</button></text>
                    </form>
                </div>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    users: state.userReducer.users
});

export default connect(
    mapStateToProps,
    { registerpage }
)(registerpage);
