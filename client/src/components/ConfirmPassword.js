import React, { Component } from "react";

export default class ConfirmPassword extends Component {
  render() {
    return (
      <div className="login">
        <div className="form">
          <form>
            <h1>CONFIRM PASSWORD</h1>
            <input type="password" placeholder="New Password" required />
            <br />
            <input type="password" placeholder="Confirm Password" required />
            <br />
            <button class="next">RESET</button>
          </form>
        </div>
      </div>
    );
  }
}
