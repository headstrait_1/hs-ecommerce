import React from "react";
import logo from "./logo.svg";
import "./App.css";
import ResetPassword from "../src/components/ResetPassword";
import { BrowserRouter as Router, Route } from "react-router-dom";
import ConfirmPassword from "./components/ConfirmPassword";

function App() {
  return (
    <div className="App">
      <Router>
        <Route exact path="/" component={ResetPassword}></Route>
        <Route exact path="/Confirm" component={ConfirmPassword}></Route>
      </Router>
    </div>
  );
}

export default App;
